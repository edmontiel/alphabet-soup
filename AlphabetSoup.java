import java.util.List;

public class AlphabetSoup {

  public static void main(String[] args) {

    //Change file name to the testing file. 
    String filename = "alphabet-soup/sample1.txt";
    FileObject fo = new FileObject(filename);

    Boolean anyWordsFound = false; 
    // Search for each word in the grid
    for (String word : fo.getWords()) {
      List<int[]> indices = WordUtil.findWord(word, fo.getGrid(), fo.getNumRows(), fo.getNumCols());
      if (!indices.isEmpty()) {
        anyWordsFound = true; 
        System.out.println(word + " " + WordUtil.formatIndices(indices));
      } else {
        // Search for the word backwards
        String reversedWord = new StringBuilder(word).reverse().toString();
        indices = WordUtil.findWord(reversedWord, fo.getGrid(), fo.getNumRows(), fo.getNumCols());
        if (!indices.isEmpty()) {
          anyWordsFound = true; 
          System.out.println(word + " " + new StringBuilder(WordUtil.formatIndices(indices)).reverse());
        }
      }
    }
    if(!anyWordsFound){
      System.out.println("No words were found in the grid");
    }
    
  }
}
