import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * The FileObject class represents a file that contains a grid of characters and a list of words to find within that grid. 
 * The constructor is used to parse the file and retrieve the grid, the list of words, and the dimensions of the grid. 
 */
public class FileObject {
    
    private List<String> words = new ArrayList<>();
    private char[][] grid = null;
    private int numRows = 0;
    private int numCols = 0;

    /**
     * constructor to parse file and assign attribute values. 
     * 
     * @param fileName The file
     */
    public FileObject(String fileName){
        Logger log = Logger.getLogger("logger");
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            // Read the first line to get the grid size
            String line = br.readLine();
            String[] parts = line.split("x");
            numRows = Integer.parseInt(parts[0]);
            numCols = Integer.parseInt(parts[1]);
      
            // Initialize the grid with appropiate size
            grid = new char[numRows][numCols];
      
            // Read the grid characters line by line
            for (int i = 0; i < numRows; i++) {
              line = br.readLine();
              parts = line.split(" ");
              for (int j = 0; j < numCols; j++) {
                grid[i][j] = parts[j].charAt(0);
              }
            }
      
            // Read the list of words to find
            while ((line = br.readLine()) != null) {
              words.add(line.replaceAll("\\s+",""));
            }
          } catch (IOException e) {
            log.log(Level.SEVERE, "File " + fileName + " Does Not Exist", e);
          }
    }

    // Getter methods. 

    public List<String> getWords() {
        return this.words;
    }

    public char[][] getGrid() {
        return this.grid;
    }

    public int getNumRows() {
        return this.numRows;
    }

    public int getNumCols() {
        return this.numCols;
    }

}
