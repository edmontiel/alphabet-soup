import java.util.ArrayList;
import java.util.List;

/*
 * Utility class to find words from a grid. Provides static methods to search for a given word in a 2D character array, 
 * horizontally and vertically, and diagonally from top-left to bottom-right and bottom-left to top-right.
 */
public class WordUtil {

    /**
    * Searches for a given word in a 2D character array, horizontally and vertically, 
    * and diagonally from top-left to bottom-right and bottom-left to top-right.
    *
    * @param word The word to search for
    * @param grid The 2D character array to search in
    * @param numRows The number of rows in the 2D character array
    * @param numCols The number of columns in the 2D character array
    * @return A list of start and end indices for each instance of the word in the grid
    */  
    public static List<int[]> findWord(
    String word,
    char[][] grid,
    int numRows,
    int numCols
  ) {

    List<int[]> indices = new ArrayList<>();
    // Search for the word horizontally
    for (int i = 0; i < numRows; i++) {
      for (int j = 0; j <= numCols - word.length(); j++) {
        boolean found = true;
        for (int k = 0; k < word.length(); k++) {
          if (grid[i][j + k] != word.charAt(k)) {
            found = false;
            break;
          }
        }
        if (found) {
          int[] start = { i, j };
          int[] end = { i, j + word.length() - 1 };
          indices.add(start);
          indices.add(end);
          return indices;
        }
      }
    }

    // Search for the word vertically
    for (int i = 0; i <= numRows - word.length(); i++) {
      for (int j = 0; j < numCols; j++) {
        boolean found = true;
        for (int k = 0; k < word.length(); k++) {
          if (grid[i + k][j] != word.charAt(k)) {
            found = false;
            break;
          }
        }
        if (found) {
          int[] start = { i, j };
          int[] end = { i + word.length() - 1, j };
          indices.add(start);
          indices.add(end);
          return indices;
        }
      }
    }

    // Search for the word diagonal, top-left to bottom-right
    for (int i = 0; i <= numRows - word.length(); i++) {
      for (int j = 0; j <= numCols - word.length(); j++) {
        boolean found = true;
        for (int k = 0; k < word.length(); k++) {
          if (grid[i + k][j + k] != word.charAt(k)) {
            found = false;
            break;
          }
        }
        if (found) {
          int[] start = { i, j };
          int[] end = { i + word.length() - 1, j + word.length() - 1 };
          indices.add(start);
          indices.add(end);
          return indices;
        }
      }
    }

    // Search for the word diagonal, bottom-left to top-right
    for (int i = word.length() - 1; i < numRows; i++) {
      for (int j = 0; j <= numCols - word.length(); j++) {
        boolean found = true;
        for (int k = 0; k < word.length(); k++) {
          if (grid[i - k][j + k] != word.charAt(k)) {
            found = false;
            break;
          }
        }
        if (found) {
          int[] start = { i, j };
          int[] end = { i - word.length() + 1, j + word.length() - 1 };
          indices.add(start);
          indices.add(end);
          return indices;
        }
      }
    }
    return indices;
  }

  /**
   * Formats a list of start and end indices as a String of "(row,col)" pairs separated by a space
   * 
   * @param indices The list of start and end indices
   * @return The formatted String
   */
  public static String formatIndices(List<int[]> indices) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < indices.size(); i++) {
      sb
        .append(indices.get(i)[0])
        .append(":")
        .append(indices.get(i)[1]);
      if (i < indices.size() - 1) {
        sb.append(" ");
      }
    }
    return sb.toString();
  }
    
}
